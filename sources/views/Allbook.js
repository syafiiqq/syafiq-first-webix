import {JetView} from "webix-jet";
import {bookdata} from "models/bookrec";
import {allbook} from "models/BookDataLoader";
import AllbookData from "./Allbookdata";
import AllbookForm from "./forms/Allbookform";
export default class AllbookView extends JetView{
	config(){
		return {
			rows:[ 
				{view:"toolbar", css:"subbar", padding:0,
			elements:[


				{
					view:"button", localId:"button:add", type:"form",
					label:"Add New Book", width:140,
					click:() => this.$$("multi").setValue("formView")
				},
				{
					view:"button", template:`<svg id="ThemeBtn"
					xmlns="http://www.w3.org/2000/svg"
					xml:space="preserve"
					width="16mm"
					height="8mm"
					version="1.1"
					style="
					  shape-rendering: geometricPrecision;
					  text-rendering: geometricPrecision;
					  image-rendering: optimizeQuality;
					  fill-rule: evenodd;
					  clip-rule: evenodd;
					"
					viewBox="0 0 1600 800"
					xmlns:xlink="http://www.w3.org/1999/xlink"
				  >
					<g id="ThemeToggleBtn">
					  <metadata id="CorelCorpID_0Corel-Layer" />
					  <path
						id="rectangle"
						class="rectangleFill rectacngleStroke"
						d="M389.9 56.04l820.2 0c189.93,0 343.96,154.03 343.96,343.96l0 0c0,189.92 -154.03,343.95 -343.96,343.95l-820.2 0.01c-189.92,-0.01 -343.95,-154.04 -343.95,-343.96l-0.01 0c0.01,-189.93 154.04,-343.96 343.97,-343.96z"
					  />
					  <circle
						id="selectCircle"
						class="circleFill"
						cx="1223.33"
						cy="400"
						r="304.27"
					  />
					  <path
						id="moon"
						class="moonFill"
						d="M389.91 383.85c0,47.74 -4.06,75.68 16.77,116.82 27.39,54.09 44.46,59.56 67.61,94.12 -39.75,0 -70.05,-0.6 -102.85,-16.69 -92.19,-45.24 -143.82,-152.04 -97.34,-246.77 20.79,-42.38 40.66,-68.51 83.72,-92.07 39.61,-21.67 58.31,-24.16 116.47,-24.16 -20.85,31.13 -36.19,37 -60.13,80.5 -0.76,1.38 -1.74,3.16 -2.49,4.55 -0.75,1.38 -1.76,3.14 -2.49,4.53 -14.69,27.76 -19.27,45.24 -19.27,79.17zm-175.79 -7.03c0,112.18 20.51,180.53 127.24,238.4 100.55,54.52 200.88,-6.13 222.14,-7.9 -9.23,-12.95 -11.07,-16.72 -27.93,-28.32 -28.58,-12.3 -32.65,-28.28 -56.23,-52.55 -62.43,-64.25 -61.67,-177.36 -3.51,-239.49l57.77 -47.71c2.25,-1.24 13.19,-7.33 13.57,-7.52 27.78,-20.78 15.45,-12.75 32.59,-30.69 -17.6,-1.47 -138.83,-59.15 -233.86,-8.86 -4.43,2.34 -10.02,5.44 -13.64,7.46 -63.83,35.72 -118.14,106.23 -118.14,177.18z"
					  />
					  <path
						id="sun"
						class="sunFill"
						d="M1070.18 387.8c0,89.18 69.56,158.74 158.74,158.74 59.56,0 141.1,-65.45 141.1,-158.74 0,-59.55 -65.45,-141.1 -158.74,-141.1 -63.49,0 -141.1,77.61 -141.1,141.1zm335.12 26.46l8.82 0 0 -35.28 -8.82 0 0 35.28zm-379.22 0l8.82 0 0 -35.28 -8.82 0 0 35.28zm167.56 194.01c0,18.57 7.1,30.32 16.1,35.28l20.72 0c9,-4.97 16.1,-16.72 16.1,-35.28 0,-37.36 -52.92,-37.36 -52.92,0zm0 -432.12c0,50.67 52.92,50.67 52.92,0 0,-14.27 -7.72,-23.09 -17.26,-26.46l-18.4 0c-9.54,3.37 -17.26,12.19 -17.26,26.46zm149.93 361.57c0,49.22 61.73,55.32 61.73,26.46 0,-49.21 -61.73,-55.32 -61.73,-26.46zm-308.67 26.46c0,28.86 61.73,22.76 61.73,-26.46 0,-28.86 -61.73,-22.75 -61.73,26.46zm308.67 -308.66c0,28.86 61.73,22.75 61.73,-26.46 0,-28.86 -61.73,-22.76 -61.73,26.46zm-308.67 -26.46c0,49.21 61.73,55.32 61.73,26.46 0,-49.22 -61.73,-55.32 -61.73,-26.46z"
					  />
					</g>
				  </svg>`, 	
				  click:() =>   document.body.classList.toggle("dark") ,

				}
			]
		},
			{
			animate:false,
			fitBiggest:true,
			localId:"multi",
			cells:[
				{ id:"gridView", $subview:AllbookData},
				{ id:"formView", $subview:AllbookForm}
			],
			on:{
				onViewChange:(prev)=>{
					const button = this.$$("button:add");
					if (prev == "gridView") button.hide();
					else button.show();
				}
			},
			on:{
				onListItemClick: function(id,e,node,list){
				  webix.message("Item '"+this.getItem(id).text+"' has been clicked.");
				  console.log("boom");
				}
			},
		}
	]
		};
	}
urlChange(){
	const id = this.getParam("id");
	if(id) this.$$("multi").setValue("formView");
	else this.$$("multi").setValue("gridView");
}

}