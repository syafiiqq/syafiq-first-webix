import {JetView} from "webix-jet";
import {bookdata} from "models/bookrec";
import {Allbook} from "models/BookDataLoader";
export default class AllbookData extends JetView{
	config(){
        return { view:"datatable",
        // save: {
        //     url:"data/books.json",
        //     autoupdate:true
        // },
        // url: "data/books.json",
		columns:[
			// { id:"id", header:"ID", adjust:true,sort:"int" },
			{ id:"title", header:[ "Title", { content:"textFilter" } ], adjust:true },
			{ id:"author", header:[ "Author", { content:"textFilter" } ], adjust:true },
            { id:"pages", header:"Pages", adjust:true, sort:"int" },
            { id:"buydate", header:"Buy Date", adjust:true, sort:"date" },
            { id:"edit", header:"", width:35, template:"{common.editIcon()}" },
            { id:"delete", header:"", width:35, template:"{common.trashIcon()}" },
            // { id:"description", header:"Description", adjust:true },
           
        ],
        
        onClick:{
            "wxi-pencil":(e, id) => {
                this.show("Allbook?id="+id.row);
                
                // console.log(Allbook.getState());
            },
            "wxi-trash":(e,id) => {
                this.webix.confirm({
                    text: "The book data will be deleted, <br/> Are you sure?",
                    ok:"Yes", cancel:"Cancel",
                    callback:(res) => {
                        if(res){
                            Allbook.remove(id);
                        }
                    }
                });
            }
        },

        select:true,
         css:"webix_shadow_medium" 
        };
	}
	init(view){
        view.sync(Allbook);
        
		//  const tetew = view.
	}
}