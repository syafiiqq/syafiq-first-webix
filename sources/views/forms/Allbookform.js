import {JetView} from "webix-jet";
import {Allbook} from "models/BookDataLoader";
export default class AllbookForm extends JetView{
    config(){
        return{
            view:"form",paddingY:20, paddingX:30,
            elementsConfig:{ labelWidth:100},
            elements: [
                { type:"header", height:45,template:"Add or edit a book", css:"formheader"},
                { view:"text", name:"title", label:"Title",invalidMessage: "Book Title can not be empty" },
                { view:"text", name:"author", label:"Author"},
                { view:"text", name:"pages", label:"Pages", invalidMessage: "Pages should only in number :)" },
                { view:"datepicker", label:"Buy Date", name:"buydate", stringResult:true, timepicker: true, },
                // { view:"text", name:"description", label:"Description"},
                { 
                    margin:10,
                    cols:[
                        { view:"button", value:"<< Back", align:"center", width:120,
                          click:() => {
                              this.getBack();
                          }
                        },
                        { view:"button", value:"Save", type:"form", align:"center", width:120,
                          click:() => {
                              const form = this.getRoot();
                            

                              if(form.validate()) {
                                this.saveBook(form.getValues());
                                this.getBack();  
                                
                              }
                          }
                        },
                        {}
                    ]
                }
            ],
            rules: {
                title: webix.rules.isNotEmpty,
                pages: webix.rules.isNumber

            }
        }
    }
    urlChange(form){
        Allbook.waitData.then(()=> {
            const id = this.getParam("id");
            form.setValues(Allbook.getItem(id));
            if( id && Allbook.exist(id)) {
                form.setValues(Allbook.getItem(id));
                console.log()
            }
        });
    }
    saveBook(values){
        console.log(values);
        const id = values.id;
        // Allbook.updateItem(id, values);
        
        if(id)   Allbook.updateItem(id, values);
        else Allbook.add(values);
    }
   
    getBack(){
        const form = this.getRoot();
        form.clear();
        form.clearValidation();
        this.show("Allbook")

    }
    
}
